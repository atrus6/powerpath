extends Control


# Called when the node enters the scene tree for the first time.
func _ready():
	$VBoxContainer/RTM0.text = Utils.commas(Highscores.million[0]) + " away"
	$VBoxContainer/RTM1.text = Utils.commas(Highscores.million[1]) + " away"
	$VBoxContainer/RTM2.text = Utils.commas(Highscores.million[2]) + " away"
	
	$VBoxContainer/OU0.text = "In " + Utils.commas(Highscores.overunder[0]) + " gates"
	$VBoxContainer/OU1.text = "In " + Utils.commas(Highscores.overunder[1]) + " gates"
	$VBoxContainer/OU2.text = "In " + Utils.commas(Highscores.overunder[2]) + " gates"
	
	$VBoxContainer/BT0.text = Utils.commas(Highscores.highest[0])
	$VBoxContainer/BT1.text = Utils.commas(Highscores.highest[1])
	$VBoxContainer/BT2.text = Utils.commas(Highscores.highest[2])


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	pass


func _on_main_menu_pressed():
	MusicController.switch_track(MusicController.track_mainmenu)
	get_tree().change_scene_to_file("res://main_menu.tscn")
