extends Node

var million = [999999999999,999999999999,999999999999]
var overunder = [999999999999,999999999999,999999999999]
var highest = [0,0,0]

var path = "user://scores.cfg"

func save_data():
	var config = ConfigFile.new()
	
	config.set_value("Million", "score0", million[0])
	config.set_value("Million", "score1", million[1])
	config.set_value("Million", "score2", million[2])
	
	config.set_value("OverUnder", "score0", overunder[0])
	config.set_value("OverUnder", "score1", overunder[1])
	config.set_value("OverUnder", "score2", overunder[2])
	
	config.set_value("Highest", "score0", highest[0])
	config.set_value("Highest", "score1", highest[1])
	config.set_value("Highest", "score2", highest[2])
	
	config.save(path)
	
func load_data():
	var config = ConfigFile.new()
	
	var err = config.load(path)
	
	if err != OK:
		return
		
	million[0] = config.get_value("Million", "score0")
	million[1] = config.get_value("Million", "score1")
	million[2] = config.get_value("Million", "score2")
	
	overunder[0] = config.get_value("OverUnder", "score0")
	overunder[1] = config.get_value("OverUnder", "score1")
	overunder[2] = config.get_value("OverUnder", "score2")
	
	highest[0] = config.get_value("Highest", "score0")
	highest[1] = config.get_value("Highest", "score1")
	highest[2] = config.get_value("Highest", "score2")
	


func add_million(score:int):
	million.append(score)
	million.sort()
	million = million.slice(0,3)
	save_data()
	
func add_overunder(score:int):
	overunder.append(score)
	overunder.sort()
	overunder = overunder.slice(0,3)
	save_data()
	
func add_highest(score:int):
	highest.append(score)
	highest.sort()
	highest.reverse()
	highest = highest.slice(0,3)
	save_data()
