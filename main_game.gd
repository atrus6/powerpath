extends Node2D

@export var gate: PackedScene
var gates_created := 0

# Called when the node enters the scene tree for the first time.
func _ready():
	create_new_gates()


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	pass

func create_new_gates():
	var left_gate = gate.instantiate()
	var right_gate = gate.instantiate()
	
	left_gate.position = Vector2(250, -89.6)
	right_gate.position = Vector2(431, -89.6)
	
	left_gate.scale = Vector2(.7, .7)
	right_gate.scale = Vector2(.7, .7)
	
	right_gate.left = true
	
	add_child(left_gate)
	add_child(right_gate)
	

func _on_timer_timeout():
	create_new_gates()
	gates_created += 1
