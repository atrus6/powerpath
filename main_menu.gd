extends Control

var explosion = preload("res://explosion.tscn")

# Called when the node enters the scene tree for the first time.
func _ready():
	Highscores.load_data()


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	pass


func _on_million_pressed():
	MusicController.switch_track(MusicController.track_million)
	get_tree().change_scene_to_file("res://million.tscn")


func _on_highscore_pressed():
	MusicController.switch_track(MusicController.track_highscores)
	get_tree().change_scene_to_file("res://highscore_menu.tscn")


func _on_tutorial_pressed():
	MusicController.switch_track(MusicController.track_tutorial)
	get_tree().change_scene_to_file("res://tutorial.tscn")


func _on_over_pressed():
	MusicController.switch_track(MusicController.track_overunder)
	get_tree().change_scene_to_file("res://over_under.tscn")


func _on_beat_pressed():
	MusicController.switch_track(MusicController.track_highest)
	get_tree().change_scene_to_file("res://highest.tscn")


func _on_level_pressed():
	var e = explosion.instantiate()
	add_child(e)


func _on_check_button_toggled(button_pressed):
	if button_pressed:
		MusicController.stop()
	else:
		MusicController.play()
