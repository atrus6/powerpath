extends Node2D

var MILLION := 1000000

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	if $level/Player.score == MILLION:
		Highscores.add_million(0)
		get_tree().change_scene_to_file("res://highscore_menu.tscn")
	if $level.gates_created >= 20:
		var score = abs(MILLION - $level/Player.score)
		Highscores.add_million(score)
		get_tree().change_scene_to_file("res://highscore_menu.tscn")
		
	$Label.text = str(20-$level.gates_created)
	
	
