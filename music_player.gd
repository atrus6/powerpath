extends Node

var players = []

var current_bus = 0

var current_pos = 0.0

var FADE_LENGTH = 2.0

var track_tutorial = "res://music/tutorial.ogg"
var track_highscores = "res://music/highscores.ogg"
var track_mainmenu = "res://music/main_menu.ogg"
var track_million = "res://music/million.ogg"
var track_overunder = "res://music/over_under.ogg"
var track_highest = "res://music/highest.ogg"

# Called when the node enters the scene tree for the first time.
func _ready():
	players.append($Player1)
	players.append($Player2)
	players.append($Player3)
	players[0].stream = load(track_mainmenu)
	players[0].play()


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	pass
	

func get_current_player() -> AudioStreamPlayer:
	return players[current_bus]
	
func next_player():
	current_bus += 1
	current_bus %= 3

func fade_out():
	var tween_vol = get_tree().create_tween()
	var current = get_current_player()
	
	tween_vol.tween_property(current, "volume_db", -50, FADE_LENGTH)
	tween_vol.tween_callback(current.stop)
	
func stop():
	current_pos = get_current_player().get_playback_position()
	get_current_player().stop()

func play():
	get_current_player().play(current_pos)
	
func switch_track(track:String):
	fade_out()
	next_player()
	var tween_vol = get_tree().create_tween()
	var current = get_current_player()
	current.stream = load(track)
	current.play()
	current.volume_db = -50
	tween_vol.tween_property(current, "volume_db", 0, FADE_LENGTH)
	


func _on_player_1_finished():
	players[0].play()


func _on_player_2_finished():
	players[1].play()


func _on_player_3_finished():
	players[2].play()
