extends Node2D

@export var SPEED := 100
var score = 1


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	var velocity:int = 0
	if Input.is_action_pressed("move_left"):
		velocity = -1
	if Input.is_action_pressed("move_right"):
		velocity = 1
		
	position.x += velocity * SPEED * delta
	$Score.text = Utils.commas(score)
	
func _input(event):
	if event is InputEventScreenTouch or \
		event is InputEventScreenDrag:
		position.x = event.position.x
		
	if event is InputEventMouseButton and event.is_pressed:
		position.x = event.position.x
	
	if Input.is_mouse_button_pressed(MOUSE_BUTTON_LEFT):
		if event is InputEventMouseMotion and event.is_pressed:
			position.x = event.position.x

func _on_area_2d_area_entered(area:Area2D):
	var value = area.get_parent().value
	var op = area.get_parent().operation
	
	if op == area.get_parent().OPERATION.ADD:
		score = score + value
	elif op == area.get_parent().OPERATION.SUBTRACT:
		score = score - value
	elif op == area.get_parent().OPERATION.MULTIPLY:
		score = score*value
	elif op == area.get_parent().OPERATION.DIVIDE:
		if value == 0:
			score = 0
		else:
			score = score/value
	
