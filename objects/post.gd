extends Node2D

@export var left:bool
@export var animation_length := 5.0
enum OPERATION {ADD, SUBTRACT, MULTIPLY, DIVIDE}
@export var operation:OPERATION = OPERATION.ADD
@export var value := 1

# Called when the node enters the scene tree for the first time.
func _ready():
	var tween_scale = get_tree().create_tween()
	var tween_move = get_tree().create_tween()
	tween_scale.tween_property(self, "scale", Vector2(1.5, 1.5), animation_length)
	if !left:
		tween_move.tween_property(self, "position", Vector2(142, 1500), animation_length)
	else:
		tween_move.tween_property(self, "position", Vector2(515, 1500), animation_length)
	tween_scale.tween_callback(self.queue_free)
	
	value = randi()%100
	operation = get_rand_operation()
	
	$Label.text = get_operation_string(operation) + str(value)
	
func get_operation_string(op):
	if op == OPERATION.ADD:
		return "+"
	elif op == OPERATION.SUBTRACT:
		return "-"
	elif op == OPERATION.MULTIPLY:
		return "×"
	elif op == OPERATION.DIVIDE:
		return "÷"

func get_rand_operation():
	return randi() % OPERATION.size()

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	pass
