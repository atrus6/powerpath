extends Node2D

var MIN := 13_000_000
var MAX := 24_000_000


# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	if $level/Player.score > MIN and $level/Player.score < MAX:
		Highscores.add_overunder($level.gates_created)
		get_tree().change_scene_to_file("res://highscore_menu.tscn")
