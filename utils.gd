extends Node

func commas(value:int) -> String:
	var neg = false
	
	if value < 0:
		neg = true
		value = abs(value)
	
	var ret:String = str(value)
	
	var i:int = ret.length() -3
	while i > 0:
		ret = ret.insert(i, ',')
		i -= 3
		
	if neg:
		return "-" + ret
	else:
		return ret
